﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour {



    

    public bool isSlotted;
    public GameObject storedObject;

    public Material SelectedGlow;
    public Material defaultMat;
    Renderer rend;
    // Use this for initialization
    void Start () {
        isSlotted = false;
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = defaultMat;
    }

    void Update()
    {

        if (InventoryManager.selectedSlot == gameObject)
        {
            gameObject.GetComponent<Image>().color = new Color32(0, 255, 0, 50);

        }

        

        if (InventoryManager.selectedSlot != gameObject)
        {
            gameObject.GetComponent<Image>().color = new Color32(255, 255, 225, 100);
        }
    }

    // Update is called once per frame

    public void SetSelected()
    {
        InventoryManager.selectedSlot = gameObject;
        InventoryManager.selectedObject = storedObject;
        Debug.Log(InventoryManager.selectedSlot);

    }
}
