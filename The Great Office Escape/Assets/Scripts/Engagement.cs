﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public abstract class Engagement : MonoBehaviour {

    protected GameManager gameManager;
    protected CircleCollider2D playerCollider;
    protected BoxCollider2D objectCollider;

    protected Transform map;
    protected Animator mapAnim;
    public GameObject canvas;
    private bool discovered = false;
    private bool destroyed = false;
    //private bool canBeDestroyed = false;

    protected bool InRange = false;

    protected StationaryObject stationScript;

    // Use this for initialization
    protected void Start ()
    {
        stationScript = gameObject.GetComponent<StationaryObject>();
        gameManager = GameObject.FindObjectOfType<GameManager>();
        map = transform.Find("bp");
        mapAnim = map.GetComponent<Animator>();

        playerCollider = GameObject.FindWithTag("Player").GetComponent<CircleCollider2D>();
        objectCollider = transform.GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }

    abstract public void Interact(); //Perform whatever action / "minigame" this object does.

    protected void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player") //If the object coming in range of this object is the player...
        {
            if (!discovered) //If this object hasn't been passed by the player...
            {
                //Show the object's icon on the map and enable the map icon's animation
                map.GetComponent<SpriteRenderer>().enabled = true;
                discovered = true;
            }

            InRange = true;
            canvas.SetActive(true);

            if (!destroyed) //If this object hasn't been destroyed...
            {
                //
                col.GetComponent<PlayerMovement>().SetNearbyInventory(gameObject.GetComponent<InventoryManager>()); //!!!!!!!!!!!!!!!!!!!
                transform.GetChild(0).GetChild(0).gameObject.SetActive(true); //!!!!!!!!!! Get the object's UI / "Canvas" and set it to visible and active
                /*if (health < 100.0f)
                {
                    col.GetComponent<PlayerSuspicion>().playerIsSuspicious = true; // The player is suspicious from being near this object and its health being low
                }*/
            }
        }
    }

    protected void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player") //If the object coming in range of this object is the player...
        {
            col.GetComponent<PlayerMovement>().SetNearbyInventory(null); //!!!!!!!!!!!!!!!!!!!
            if (!destroyed) //If this object hasn't been destroyed...
            {
                transform.GetChild(0).GetChild(0).gameObject.SetActive(false); //!!!!!!!!!! Get the object's UI / "Canvas" and set it to invisible and not active
            }
            col.GetComponent<PlayerSuspicion>().playerIsSuspicious = false; // The player is no longer suspicious from being near this object.

            InRange = false;
            canvas.SetActive(false);
        }
    }

    public bool GetDestroyedStatus()
    {
        return destroyed;
    }

    public void SetDestroyedStatus(bool newStatus)
    {
        destroyed = newStatus;
    }

    /*public bool GetDestroyableStatus()
    {
        return canBeDestroyed;
    }

    public void SetDestroyableStatus(bool newStatus)
    {
        canBeDestroyed = newStatus;
    }*/

}
