﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOVMesh : MonoBehaviour {

	FOW fow;
	Mesh mesh;
	RaycastHit2D hit;
	[SerializeField] float meshRes = 2;
	[HideInInspector] Vector3[] vertices;
	[HideInInspector] public int[] triangles;
	[HideInInspector] public int stepCount;

	// Use this for initialization
	void Awake () {
		mesh = GetComponent<MeshFilter> ().mesh;
		fow = GetComponentInParent<FOW> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		MakeMesh ();
	}

	void MakeMesh(){
		stepCount = Mathf.RoundToInt (fow.viewAngle * meshRes);
		float stepAngle = fow.viewAngle / stepCount;

		List<Vector3> viewVertex = new List<Vector3> ();

		hit = new RaycastHit2D ();

		for (int i = 0; i <= stepCount; i++) {
			float angle = fow.transform.eulerAngles.y - fow.viewAngle / 2 + stepAngle * i;
			Vector3 dir = fow.DirFromAngle (angle, false);
			hit = Physics2D.Raycast (fow.transform.position, dir, fow.viewRadius, fow.obstacleMask);
			if (hit.collider == null) {
				viewVertex.Add (transform.position + dir.normalized * fow.viewRadius);
			} else {
				viewVertex.Add (transform.position + dir.normalized * hit.distance);
			}
		}

			int vertexCount = viewVertex.Count + 1;
			vertices = new Vector3[vertexCount];
			triangles = new int[(vertexCount - 2) * 3];

			vertices [0] = Vector3.zero;

			for (int j = 0; j < vertexCount - 1; j++){
				vertices[j + 1] = transform.InverseTransformPoint(viewVertex[j]);
				if (j < vertexCount - 2) {
				triangles [j * 3 + 2] = 0;
				triangles [j + 2] = j + 1;
				triangles [j * 3] = j + 2;
			}
		}

			mesh.Clear();
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.RecalculateNormals();
	}
}