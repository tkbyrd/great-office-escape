﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteractions : MonoBehaviour {
    
    public bool canInteract = false;
    public GameObject collidingObject = null;

    public GameObject InteractedObject = null;
    public StationaryObject StationaryScript = null;
    public Item itemScript = null;

    private Sprite ItemSprite;
    

    public List<GameObject> InventorySlots;

    void Start ()
    {
        Debug.Log(InventoryManager.instance.items.Count);
    }
	
	void Update ()
    {

     
          

       

        //print(collidingObject != null);
        if (Input.GetKeyDown(KeyCode.E) && collidingObject != null)
        {
            if (StationaryScript.tampered == false)
            {
                foreach (GameObject item in InventoryManager.instance.items)
                {
                    if (item == InventoryManager.selectedObject) { 
                 
                    print(CheckIfCompatible(item.GetComponent<Item>()));
                        if (CheckIfCompatible(item.GetComponent<Item>()))

                        {

                            SpriteChange(item.GetComponent<Item>());

                            //Insert all sound code here *******************************************************************************




                            StationaryScript.tampered = true;
                            InventoryManager.instance.Remove(item);
                            //StationaryScript.engagement.SetDestroyableStatus(true);
                            Debug.Log("These two objects can interact!");
                            InventoryManager.instance.items.Remove(item);
                            break;
                        }
                    }
                }
            }
            /*foreach (Item item in InventoryManager.instance.items)
            {
                print(item);
                print(InventoryManager.instance.items[0]);
                Debug.Log("Checking if " + item + " is compatible with " + InteractedObject);
                if (CheckIfCompatible(item))
                 // if(itemScript.liquid == true && StationaryScript.dirty == true)
                {
                    StationaryScript.engagement.SetDestroyableStatus(true);
                    Debug.Log("These two objects can interact!");
                }
            }*/

            UpdateInventory();
        }
        if (Input.GetKeyDown(KeyCode.R) && collidingObject != null)
        {
            if (collidingObject.gameObject.tag == "Pickup")
            {
                InventoryManager.instance.items.Add(collidingObject.gameObject);
                Debug.Log(InventoryManager.instance.items.Count);
                collidingObject.SetActive(false);
                //collidingObject = null;*/

                foreach (GameObject slot in InventorySlots)
                {
                    slot.SetActive(false);
                }
               

            }


            /*
            foreach (GameObject item in InventoryManager.instance.items)
            {
                ItemSprite = item.GetComponent<SpriteRenderer>().sprite;
                //Debug.Log(item);
                foreach (GameObject slot in InventorySlots)
                {
                    if (slot.activeSelf == false)
                    {
                        slot.SetActive(true);
                        slot.GetComponent<Image>().sprite = ItemSprite;
                        break;
                    }
                }

            }
            */
            UpdateInventory();


        }

        
    }



    void OnTriggerEnter2D(Collider2D other)
    {

        canInteract = true;
        collidingObject = other.gameObject;
        if (other.gameObject.tag == "stationary")
        {
            
            InteractedObject = other.gameObject;
            StationaryScript = InteractedObject.GetComponent<StationaryObject>();
            Debug.Log(InteractedObject);

            //StationaryScript.engagement.SetDestroyableStatus(true);




        }
        if (other.gameObject.tag == "Pickup")
        {

            InteractedObject = other.gameObject;
            itemScript = InteractedObject.GetComponent<Item>();
            Debug.Log(InteractedObject);






        }



    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag == "Pickup")
        {
            itemScript = null;
        }
        if (other.gameObject.tag == "stationary")
        {
            StationaryScript = null;
        }
        if(other.gameObject == collidingObject)
        {
            collidingObject = null;
            canInteract = false;
        }

        if (other.gameObject == InteractedObject)
        {
            //StationaryScript.engagement.SetDestroyableStatus(false);
            InteractedObject = null;
        }

    }

    public bool CheckIfCompatible(Item pickup)
    {
        if (pickup.writing && StationaryScript.dirty)
        {

            return true;
        }
        if (pickup.sharp && (StationaryScript.cuttable || StationaryScript.electronic))
        {

            return true;
        }
        if (pickup.liquid && (StationaryScript.dirty || StationaryScript.electronic || StationaryScript.flammable))
        {

            return true;
        }
        if (pickup.ignitor && StationaryScript.flammable)
        {

            return true;
        }
        else
        {

            return false;
        }
    }

    public void SpriteChange(Item pickup)
    {
        if (pickup.writing && StationaryScript.dirty)
        {

            InteractedObject.GetComponent<SpriteRenderer>().sprite = StationaryScript.WrittenSprite;
        }
        if (pickup.sharp && (StationaryScript.cuttable || StationaryScript.electronic))
        {

            InteractedObject.GetComponent<SpriteRenderer>().sprite = StationaryScript.CutSprite;
        }
        if (pickup.liquid && (StationaryScript.dirty || StationaryScript.electronic || StationaryScript.flammable))
        {

            InteractedObject.GetComponent<SpriteRenderer>().sprite = StationaryScript.WetSprite;
        }
        if (pickup.ignitor && StationaryScript.flammable)
        {

            InteractedObject.GetComponent<SpriteRenderer>().sprite = StationaryScript.BurnedSprite;
        }
        else
        {

            
        }
    }

    void UpdateInventory()
        {

        foreach (GameObject slot in InventorySlots)
        {
            slot.SetActive(false);
        }


            foreach (GameObject item in InventoryManager.instance.items)

        
        {
            ItemSprite = item.GetComponent<SpriteRenderer>().sprite;
            //Debug.Log(item);
            foreach (GameObject slot in InventorySlots)
            {
                if (slot.activeSelf == false)
                {
                    slot.SetActive(true);
                    slot.GetComponent<Image>().sprite = ItemSprite;
                    slot.GetComponent<InventorySlot>().storedObject = item;
                    break;
                    
                }
            }

        }
    }




}



