﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;

public class EnemyAI : MonoBehaviour {

    /* ----- Co-worker Attributes ----- */
    //These variables represent how fast the worker's needs for these things regenerates.
    //Whichever rate is the fastest will occur most often.
    public float workRate = 1.0f;
    public float bathroomRate = .5f;
    public float socialRate = .75f;
    public float lunchRate = .75f;

    public float quotaOfHours = 100.0f;
    public float hoursWorked = 0.0f;

    //These variables represent the current need of the worker.
    //Whichever is highest is the one that's tended to.
    private float workNeed = 10.0f;
    private float bathroomNeed = 5.0f;
    private float socialNeed = 4.0f;
    private float lunchNeed = 4.0f;

    /* ----- Player Detection ----- */
    private GameObject player;
    SpriteRenderer patrolSpriteRenderer;
    private Color patrolingColor = Color.black;
    private Color warningColor = Color.red;
    private bool inside;
    private bool isSuspicious = false;
    private float suspicion = 0f;
    private float cooldown;
    public GameObject followPrefab;
    private float timeBtwSpawns;
    public float startTimeBtwSpawns;

    /* ----- Patrol Locations ----- */
    private List<Transform> workPoints;
    private List<Transform> bathroomPoints;
    private List<Transform> socialPoints;
    private List<Transform> lunchPoints;

    public Transform home; //Includes the worker's specific desk location. This is added to workPoints.

    AIDestinationSetter destinationSetter;
    private Transform currentDestination;
    private float taskProgress;
    private float taskSpeed;
    private List<Transform> needToVisit;

    /* ----- Aesthetics ----- */
    private Image thoughtImage;
    private Slider EnemySlider;
    public Sprite coffee;
    public Sprite printer;
    public Sprite work;
    public Sprite restroom;
    public Sprite chat;
    public Sprite supplies;
    public Sprite warning;
    private Sprite currentSprite;

    PlayerSuspicion patrolSus;

    private float timeBtwRotate;
    private float rightRotateTime;
    private float leftRotateTime;
    private float rightRotateTime2;
    private AIPath aipath;
    
    





    // Use this for initialization
    void Start ()
    {

        patrolSus = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerSuspicion>();

        //Ignore other coworkers.
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Physics2D.IgnoreCollision(enemy.GetComponent<CircleCollider2D>(), transform.GetComponent<CircleCollider2D>());
        }

        /* ----- Player Detection ----- */
        
        player = GameObject.FindWithTag("Player");
        

        /* ----- Patrol Locations ----- */
        currentDestination = home;
        destinationSetter = transform.GetComponent<AIDestinationSetter>();

        workPoints = new List<Transform>();
        bathroomPoints = new List<Transform>();
        socialPoints = new List<Transform>();
        lunchPoints = new List<Transform>();

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("workPoint"))
        {
            workPoints.Add(go.GetComponent<Transform>());
        }
        workPoints.Add(home);
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("bathroomPoint"))
        {
            bathroomPoints.Add(go.GetComponent<Transform>());
        }
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("socialPoint"))
        {
            socialPoints.Add(go.GetComponent<Transform>());
        }
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("lunchPoint"))
        {
            lunchPoints.Add(go.GetComponent<Transform>());
        }
        destinationSetter.target = currentDestination;
        taskSpeed = Random.Range(5f, 50f);

        /* ----- Aesthetics ----- */
        patrolSpriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        thoughtImage = transform.GetChild(1).GetChild(0).transform.GetComponent<Image>();
        thoughtImage.enabled = true;
        thoughtImage.sprite = work;
        warningColor.a = 0f;
        patrolingColor.a = 0f;
        patrolSpriteRenderer.color = patrolingColor;
        EnemySlider = transform.GetChild(1).GetChild(1).transform.GetComponent<Slider>();
        currentSprite = work;

        timeBtwRotate = Random.Range(2f, 10f);
        aipath = transform.GetComponent<AIPath>();
        rightRotateTime = .5f;
        leftRotateTime = 1f;
        rightRotateTime2 = .5f;
        
    }








    // Update is called once per frame
    void Update()
    {
        //Update how much the worker needs social activity, work activity, bathroom break, or lunch break.
        UpdateNeeds();

        if (timeBtwRotate > 0)
        {
            aipath.canMove = true;
            timeBtwRotate -= Time.deltaTime;
        }
        else if (rightRotateTime > 0)
        {
            aipath.canMove = false;
            transform.Rotate(Vector3.forward * Time.deltaTime * -180f);
            rightRotateTime -= Time.deltaTime;
        }
        else if (leftRotateTime > 0)
        {
            aipath.canMove = false;
            transform.Rotate(Vector3.forward * Time.deltaTime * 180f);
            leftRotateTime -= Time.deltaTime;
        }
        else if (rightRotateTime2 > 0) {
            aipath.canMove = false;
            transform.Rotate(Vector3.forward * Time.deltaTime * -180f);
            rightRotateTime2 -= Time.deltaTime;
        }
        else
        {
            timeBtwRotate = Random.Range(2f, 10f);
            rightRotateTime = .5f;
            rightRotateTime2 = .5f;
            leftRotateTime = 1f;
        }

        /* ----- Canvas Position ----- */
        Vector3 myTransform = transform.position;
        myTransform.y += 1f;
        transform.GetChild(1).transform.position = myTransform;
        transform.GetChild(1).transform.rotation = Quaternion.identity;

        /* ----- Player Detection ----- */

        if (player.GetComponent<PlayerMovement>().hidden)
        {
            inside = false;
        }
        transform.GetComponent<CircleCollider2D>().enabled = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, player.transform.position - transform.position);
        if (hit.collider != null)
        {
            if (hit.transform.tag == "Player" && inside == true && player.GetComponent<PlayerSuspicion>().playerIsSuspicious)
            {
                patrolSus.PlayerHasBeenSeen();
                IndSuspicion(); //This is where suspicion increases
                string name = currentDestination.name;
                /*if ((name == "Center" || name == "Center1" || name == "Center2" || name == "Center3"))
                {
                    GameObject follow = Instantiate(followPrefab, player.transform.position, Quaternion.identity);
                    destinationSetter.target = follow.transform;
                    Destroy(follow, 8f);
                    timeBtwSpawns = startTimeBtwSpawns;

                }*/
            }
        }
        transform.GetComponent<CircleCollider2D>().enabled = true;
        if (timeBtwSpawns > 0)
        {
            timeBtwSpawns -= Time.deltaTime;
        }

        /* ----- Enemy Suspicion ---- */
        if (suspicion > 0)
        {
            thoughtImage.sprite = warning;
            patrolSpriteRenderer.color = warningColor;
            isSuspicious = true;
            EnemySlider.value = suspicion;
            EnemySlider.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.red;
            cooldown -= Time.deltaTime;

            if (cooldown < 0f)
            {
                suspicion -= Time.deltaTime * 30f;
            }
            //IF the coworker is headed to socialize, set the destination to the player.
            if (currentDestination.gameObject.tag == "socialPoint")
            {
                destinationSetter.target = player.transform;
            }
        }
        else
        {
            thoughtImage.sprite = currentSprite;
            patrolSpriteRenderer.color = patrolingColor;
            isSuspicious = false;
            EnemySlider.value = taskProgress;
            EnemySlider.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = Color.blue;
        }


        /* ----- Patrol Locations ----- */

        if (!isSuspicious) {
            if (Vector3.Distance(transform.position, currentDestination.position) < 1.1f)
            {
                taskProgress += Time.deltaTime * taskSpeed;
            }
            if (taskProgress > 100f)
            {
                taskSpeed = Random.Range(5f, 50f);
                if (Mathf.Max(workNeed, bathroomNeed, socialNeed, lunchNeed) == workNeed)
                {
                    SetNextDestination("work");
                    workNeed = 0;
                    hoursWorked += taskSpeed / 10.0f;
                }
                else if (Mathf.Max(workNeed, bathroomNeed, socialNeed, lunchNeed) == bathroomNeed)
                {
                    SetNextDestination("bathroom");
                    bathroomNeed = 0;
                }
                else if (Mathf.Max(workNeed, bathroomNeed, socialNeed, lunchNeed) == socialNeed)
                {
                    SetNextDestination("social");
                    socialNeed = 0;
                }
                else
                {
                    SetNextDestination("lunch");
                    lunchNeed = 0;
                }

                taskProgress = 0;
                /*if (!afk)
                {
                    needToComplete = Random.Range(1, 4);
                    AFK(needToComplete);

                }
                if (needToComplete - 1 == -1)
                {
                    afk = false;
                    currentDestination = home;
                    taskProgress = 0;
                }
                else
                {
                    needToComplete -= 1;
                    currentDestination = needToVisit[needToComplete];
                    taskProgress = 0;
                }*/
                taskSpeed = Random.Range(5f, 50f);
            }
            SetDestSprite();
            destinationSetter.target = currentDestination;

        }
    }
        
    private void UpdateNeeds()
    {
        workNeed += workRate;
        bathroomNeed += bathroomRate;
        socialNeed += socialRate;
        lunchNeed += lunchRate;
    }

    public void SetNextDestination(string destName)
    {

        if (destName.Equals("work"))
        {
            currentDestination = workPoints[Random.Range(0, workPoints.Count)];
        }
        else if (destName.Equals("bathroom"))
        {
            currentDestination = bathroomPoints[Random.Range(0, bathroomPoints.Count)];
        }
        else if (destName.Equals("social"))
        {
            currentDestination = socialPoints[Random.Range(0, socialPoints.Count)];
        }
        else if (destName.Equals("lunch"))
        {
            currentDestination = lunchPoints[Random.Range(0, lunchPoints.Count)];
        }
        else
        {
            currentDestination = home;
        }
            
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            inside = true;
            print("Player has stepped into the sight of the coworker.");
            patrolSus.PlayerHasBeenSeen();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            inside = false;
            patrolSus.PlayerIsNoLongerSeen();
        }

    }

    public void IndSuspicion()
    {
        if (suspicion < 100f)
        {
            suspicion += Time.deltaTime * 50f;
        }
        cooldown = 1f;
        isSuspicious = true;
    }



    /*public void AFK(int trips) {
        needToVisit = new List<Transform>();

        while (needToVisit.Count < trips) {
            Transform posTarget = pPoints[Random.Range(0, pPoints.Count)];
            if (!needToVisit.Contains(posTarget))
            {
                needToVisit.Add(posTarget);
            }
        }
        afk = true;

    }*/

    public void SetDestSprite() {

        string name = currentDestination.name;
        if (name == "Coffee")
        {
            currentSprite = coffee;
        }
        else if (name == "Supplies")
        {
            currentSprite = supplies;
        }
        else if (name == "Center" || name == "Center1" || name == "Center2" || name == "Center3")
        {
            currentSprite = chat;
        }
        else if (name == "Printer1" || name == "Printer2")
        {
            currentSprite = printer;
        }
        else if (name == "BoysBath" || name == "GirlsBath" || name == "BoysBath1" || name == "GirlsBath1" || name == "BoysBath2" || name == "GirlsBath2")
        {
            currentSprite = restroom;
        }
        else if (currentDestination = home)
        {
            currentSprite = work;
        }


    }
}
