﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class PlayerMovement : MonoBehaviour {

    //Movement Variables
    public float speed;
    private Rigidbody2D rb;
    private Vector2 moveVelocity;
    public int tp;
    private Text text;
    public bool hidden = false;
    
    PlayerSuspicion patrolSus;

    public GameObject inputField;
    private bool inputActive;
    private InventoryManager nearbyInventory = null;

    

    private void Start()
    {
        patrolSus = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerSuspicion>();
        rb = GetComponent<Rigidbody2D>();
        text = transform.GetChild(0).GetChild(0).GetComponent<Text>();
        
    }

    private void Update()
    {
        inputActive = inputField.GetComponent<InputField>().isFocused;
        if (!inputActive)
        {
            MovePlayer();
        }
        text.text = string.Concat(tp, "\nTP");
        
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    private void MovePlayer()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * speed;
    }

    public void ToggleHidden()
    {
        hidden = !hidden;

        if (hidden)
        {
            patrolSus.PlayerIsNoLongerSeen();
            gameObject.SetActive(false);
            enabled = false;
        }
        else
        {
            gameObject.SetActive(true);
            enabled = true;
        }

    }

    public void SetNearbyInventory(InventoryManager obj)
    {
        nearbyInventory = obj;
        print("Player has accquired access to a nearby inventory.");
    }

    public InventoryManager GetNearbyInventory()
    {
        return nearbyInventory;
    }

}
