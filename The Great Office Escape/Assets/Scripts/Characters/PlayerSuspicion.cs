﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerSuspicion : MonoBehaviour {

    //At different suspicion levels, have different effects for enemies.

    //PUBLIC VALUES - RATES AND MAXIMUMS
    public Image strike1;
    public Image strike2;
    public Image strike3;
    public TextMeshProUGUI cooldownText;
    public TextMeshProUGUI gameOverText; //Assign in inspector.
    public static int maxStrikes = 3; //Able to be assigned in inspector. Max number of strikes the player can get before losing.

    //PRIVATE VALUES - SUSPICION, LEVEL, AND STRIKES
    private int curStrike = 0; //The current number of strikes the player has.
    public bool playerIsSuspicious = false; //Is the player currently doing something suspicious, in a restricted area, or the like?
    private bool playerIsSeen = false;
    private bool lostGame = false;

    private float cooldownTime = 5.0f;

    //Initializes the strikeThreshold array.
    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        cooldownTime -= 1.0f * Time.deltaTime;
        //cooldownText.text = "Strike Cooldown: " + cooldownTime;

        if(playerIsSuspicious && playerIsSeen)
        {
            IncreaseStrikes();
        }
        else if (lostGame && Input.GetKeyDown("j"))
        {
            RestartGame();
        }
        CheckNumStrikes();
	}

    //Decrease the number of strikes.
    public void DecreaseStrikes()
    {
        curStrike -= 1;
        
    }

    //Increase the number of strikes.
    public void IncreaseStrikes()
    {
        if(cooldownTime <= 0.0)
        {
            curStrike += 1;
            cooldownTime = 30.0f;

        }

        if(curStrike == 1)
        {
            strike1.GetComponent<Image>().enabled = true;
        }
        else if(curStrike == 2)
        {
            strike2.GetComponent<Image>().enabled = true;
        }
        else if(curStrike == 3)
        {
            strike3.GetComponent<Image>().enabled = true;
        }
        
    }

    //Check and take action if the number of strikes is above the maximum.
    public void CheckNumStrikes()
    {
        if (curStrike >= maxStrikes) //If the suspect value reaches above the maximum allowed amount...
        {
            print("You've gotten " + curStrike + " strikes! You got fired...");
            LoseGame();
        }
    }
    
    public void PlayerHasBeenSeen()
    {
        playerIsSeen = true;
        //print("Player is being seen!");
    }

    public void PlayerIsNoLongerSeen()
    {
        playerIsSeen = false;
    }

    public void LoseGame()
    {
        lostGame = true;
        Time.timeScale = 0;
        gameOverText.SetText("Game Over - Press J to try again!");
    }

    public void RestartGame()
    {
        //Needs extension - to restart, all base values need to be reset, therefore each class needs to have a "reset" function setting values back to their original state.
        SceneManager.LoadScene("NewLayout-Jordan");
        curStrike = 0;
    }

}
