﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class printer_Manager : MonoBehaviour {


    //Testing
    public GameManager gameManager;
    //private InventoryManager printerInventory;
    public bool canBeDestroyed = false;
    /*public int index = 0;
    public GameObject blueprintInput;
    public GameObject blueprintOutput;*/


    public float health = 100f;
    private CircleCollider2D playerCollider;
    BoxCollider2D printCollider;
    public float healthRate = 15f;
    private bool destroyed = false;
    private bool hasSFXplayed = false;
    private bool hasRustlingPlayed = false;
    public Slider slider;
    

    private Transform bp;
    private Animator bpAnim;
    private bool discovered = false;

    Gradient gradient;
    GradientColorKey[] colorKey;
    GradientAlphaKey[] alphaKey;

    void Awake()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        //inventoryManager = new InventoryManager();
        //slider = GameObject.FindObjectOfType<Slider>();
    }
    // Use this for initialization
    void Start()
    {
        //printerInventory = new InventoryManager();
        //print(printerInventory.slots.Length);


        bp = transform.Find("bp");
        bpAnim = bp.GetComponent<Animator>();

        playerCollider = GameObject.FindWithTag("Player").GetComponent<CircleCollider2D>();
        printCollider = transform.GetComponent<BoxCollider2D>();
        gradient = new Gradient();

        colorKey = new GradientColorKey[4];
        colorKey[0].color = Color.red;
        colorKey[0].time = 0.05f;
        colorKey[1].color = new Color(1.0F, 0.65F, 0F);
        colorKey[1].time = 0.35f;
        colorKey[2].color = Color.yellow;
        colorKey[2].time = 0.65f;
        colorKey[3].color = Color.green;
        colorKey[3].time = 0.95f;

        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);
    }

    // Update is called once per frame
    void Update() {

        /*if(GetComponent<InventoryManager>().HasItem("stapler"))
        {
            print(GetComponent<InventoryManager>().HasItem("stapler"));
            canBeDestroyed = true;
            
        }
        else
        {
            canBeDestroyed = false;
        }*/

        /*if (transform.childCount <= 0)
        {
            inventoryManager.isFull[index] = false;
        }*/

        slider = transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<Slider>();
        slider.value = health;
        transform.GetChild(0).GetChild(0).GetChild(3).GetChild(0).GetChild(0).GetComponent<Image>().color = gradient.Evaluate(slider.value / 100f);
        if (health < 100f && !destroyed && health > 0)
        {
            health += Time.deltaTime * healthRate;
        }
        else if (health <= 0 && !destroyed)
        {
            //Stop Toolbox Rustling SFX
            /*if (hasRustlingPlayed == true && FindObjectOfType<AudioManager>().IsSFXPlaying("ToolboxRustling") == true)
            {
                FindObjectOfType<AudioManager>().Stop("ToolboxRustling");
                Debug.Log("toolboxrustling has stopped");
            }

            //Play Toner Explosion SFX & Update Text
            if (hasSFXplayed == false && FindObjectOfType<AudioManager>().IsSFXPlaying("TonerExplosion") == false)
            {
                FindObjectOfType<AudioManager>().Play("TonerExplosion");
                hasSFXplayed = true;

            }
            else return;*/

            destroyed = true;
            bpAnim.enabled = false;
            bp.transform.localScale = new Vector3(.8f, .8f, 0);

            transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
            transform.GetChild(0).GetChild(0).gameObject.SetActive(false);


        } else
        {

            health = 100f;
        }

        if (playerCollider.bounds.Intersects(printCollider.bounds))
        {
            if (health < 100.0f)
            {
                playerCollider.GetComponent<PlayerSuspicion>().playerIsSuspicious = true;
            }
            if (Input.GetKeyDown("e") && canBeDestroyed)
            {
                health -= 5f;
                /*//Play ToolboxRustling if it isn't already playing
                if (hasRustlingPlayed == false && FindObjectOfType<AudioManager>().IsSFXPlaying("ToolboxRustling") == false)
                {
                    FindObjectOfType<AudioManager>().Play("ToolboxRustling");
                    hasRustlingPlayed = true;
                }
                else return;*/

            }
        }


    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            if (!discovered)
            {
                bp.GetComponent<SpriteRenderer>().enabled = true;
                discovered = true;
            }

            if (!destroyed)
            {
                print("The player is near the printer.");
                col.GetComponent<PlayerMovement>().SetNearbyInventory(gameObject.GetComponent<InventoryManager>());
                transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                if (health < 100.0f)
                {
                    col.GetComponent<PlayerSuspicion>().playerIsSuspicious = true;
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {

        if (col.gameObject.tag == "Player")
        {
            col.GetComponent<PlayerMovement>().SetNearbyInventory(null);
            if (!destroyed)
            {
                transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
            }
            col.GetComponent<PlayerSuspicion>().playerIsSuspicious = false;
        }
    }

    public void ReturnItem(GameObject buttonPressed)
    {
        InventoryManager playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryManager>();
        /*for (int i = 0; i < playerInventory.slots.Length; i++)
        {
            if (playerInventory.isFull[i] == false)
            {
                playerInventory.isFull[i] = true;
                Instantiate(buttonPressed, playerInventory.slots[i].transform, false);
                Destroy(gameObject);
                break;
            }
        }*/
        
    }
}
