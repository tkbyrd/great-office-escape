﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GameManager : MonoBehaviour {

    //UI LABELS
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timeText; 

    private Camera BPcam; //Blueprint Camera

    public float timeLeft = 120.0f; //120 Seconds
    public bool hasWaterCup = false;
    public int score = 0;
    private bool paused = false;

    [FMODUnity.EventRef]
    public string BGMusicLoopString = "event:/BGMusicLoop"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance BGMusicLoopEv; //creating an instance of the FMOD event

    [FMODUnity.EventRef]
    public string BGNoiseLoopString = "event:/BGNoiseLoop"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance BGNoiseLoopEv; //creating an instance of the FMOD event

    // Use this for initialization
    void Start ()
    {
        BGMusicLoopEv = FMODUnity.RuntimeManager.CreateInstance(BGMusicLoopString);
        BGMusicLoopEv.start();
        BGNoiseLoopEv = FMODUnity.RuntimeManager.CreateInstance(BGNoiseLoopString);
        BGNoiseLoopEv.start();

        BPcam = GameObject.FindGameObjectWithTag("BlueprintCam").GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        timeLeft -= 1.0f * Time.deltaTime;
        scoreText.text = "Cash: $" + score;
        timeText.text = "Time Remaining: " + Mathf.RoundToInt(timeLeft) + " seconds";

        if (timeLeft < 0) 
        {
            //Game Over
        }

        if (Input.GetKeyDown("m"))
        {
            BPcam.enabled = !BPcam.enabled;
        }
    }
    
    public void GameOver()
    {
        //Game over function - reset all variables.
    }
}
