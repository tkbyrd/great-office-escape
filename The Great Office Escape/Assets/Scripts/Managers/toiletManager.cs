﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class toiletManager : MonoBehaviour {

    public float health = 0f;
    public float origHealthRate = 65f;
    private float healthRate;
    public float rateRange = 5f;
    private bool destroyed = false;
    Slider slider;
    private int localTp;
    PlayerMovement playerMovement;
    private GameManager gameManager;
    private bool direction = true;
    private bool active =false;

    private Transform bp;
    private Animator bpAnim;
    private bool discovered = false;

    Gradient gradient;
    GradientColorKey[] colorKey;
    GradientAlphaKey[] alphaKey;

    void Awake()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }
    // Use this for initialization
    void Start()
    {
        bp = transform.Find("bp");
        bpAnim = bp.GetComponent<Animator>();

        healthRate = origHealthRate;

        playerMovement = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
        gradient = new Gradient();

        colorKey = new GradientColorKey[4];
        colorKey[0].color = Color.red;
        colorKey[0].time = 0.0f;
        colorKey[1].color = new Color(1.0F, 0.65F, 0F);
        colorKey[1].time = 0.45f;
        colorKey[2].color = Color.yellow;
        colorKey[2].time = 0.92f;
        colorKey[3].color = Color.green;
        colorKey[3].time = .93f;

        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e") && active && localTp > 0)
        {
            FindObjectOfType<AudioManager>().Play("ToiletFlush");
            if (health > 92f)
            {
                //play environmental sound: WaterOverflow
                if (FindObjectOfType<AudioManager>().IsSFXPlaying("WaterOverflow") == false)
                {
                    FindObjectOfType<AudioManager>().Play("WaterOverflow");
                }

                bpAnim.enabled = false;
                bp.transform.localScale = new Vector3(.8f, .8f, 0);
                destroyed = true;
                transform.GetChild(1).gameObject.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
            }
            playerMovement.tp -= 1;
            localTp = playerMovement.tp;
            if (localTp < 1)
            {
                transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        localTp = playerMovement.tp;

        slider = transform.GetChild(0).GetChild(0).GetComponent<Slider>();
        slider.value = health;
        transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().color = gradient.Evaluate(slider.value / 100f);
        
        if (!destroyed && localTp > 0 && active)
        {
            health += Time.deltaTime * healthRate;
            if (health > 100f || health < 0f)
            {
                if (health > 100f)
                {
                    direction = false;
                }
                else if (health < 0){
                    direction = true;
                }
                
                healthRate = Random.Range(origHealthRate-rateRange, origHealthRate+rateRange);
                if (!direction) {
                    healthRate = -healthRate;
                }

            }
            
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!discovered && col.gameObject.tag == "Player")
        {
            bp.GetComponent<SpriteRenderer>().enabled = true;
            discovered = true;
        }
        if (col.gameObject.tag == "Player" && !destroyed && localTp == 0)
        {
            transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "Need something to flush!";
            transform.GetChild(0).gameObject.SetActive(true);
            health = 0;
        }
        if (col.gameObject.tag == "Player" && !destroyed && localTp > 0)
        {
            transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "Press E when green!";
            transform.GetChild(0).gameObject.SetActive(true);
            health = 0;
            active = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            transform.GetChild(0).gameObject.SetActive(false);
            active = false;
        }
    }
}
