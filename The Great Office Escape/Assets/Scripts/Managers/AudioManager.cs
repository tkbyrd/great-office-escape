﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public Sound[] sounds;

    public static AudioManager instance;
    
	void Awake ()
    {
        //used to switch between scenes || ensures multiple AudioManagers don't exist at the same time
        if (instance == null) 
            
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

	    foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }	
	}

    void Start()
    {
        Play("BG Noise");
        Play("BG Music");
        
    }
	
	public void Play (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + "not found!");
            return;
        }
            

        s.source.Play();
        
	}

    public void Stop (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + "not found!");
            return;
        }

        s.source.Stop();
        
    }

    public bool IsSFXPlaying(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s.source.isPlaying)
        {
            return true;
        }
        else
            return false;
    }

    public bool IsSoundStillPlaying (string name, float timePlaying)
    {
        //print("Checking if the sound's playing.");
        Sound s = Array.Find(sounds, sound => sound.name == name);
        //("Length of sound:" + s.clip.length);
        return timePlaying >= s.clip.length;
    }
}
