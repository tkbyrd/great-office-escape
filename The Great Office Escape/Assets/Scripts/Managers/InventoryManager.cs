﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{

    #region Singleton

    public static InventoryManager instance;
    
    void Awake()
    {
    

        instance = this;
    }

    #endregion

    public static GameObject selectedObject;
    public static GameObject selectedSlot;
 


    // Callback which is triggered when
    // an item gets added/removed.
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 5;  // Amount of slots in inventory

    public List<GameObject> InventorySlots;

    private InventorySlot slotScript = null;

    // Current list of items in inventory
    public List<GameObject> items = new List<GameObject>();

    // Add a new item. If there is enough room we
    // return true. Else we return false.
    public bool Add(GameObject item)
    {
       
        
            // Check if out of space
            if (items.Count >= space)
            {
                Debug.Log("Not enough room.");
                return false;
            }

            items.Add(item);    // Add item to list
            
            // Trigger callback
            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        

        return true;
    }

    // Remove an item
    public void Remove(GameObject item)
    {
        items.Remove(item);     // Remove item from list

        // Trigger callback
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }
    
}
