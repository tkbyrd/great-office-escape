﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationaryObject : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string pourString = "event:/Pour"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance PourEv; //creating an instance of the FMOD event

    [FMODUnity.EventRef]
    public string FireIgniteString = "event:/FireIgnite"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance FireIgniteEv; //creating an instance of the FMOD event

    [FMODUnity.EventRef]
    public string FireSustainString = "event:/FireSustain"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance FireSustainEv; //creating an instance of the FMOD event

    [FMODUnity.EventRef]
    public string CarveBoardString = "event:/CarveBoard"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance CarveBoardEv; //creating an instance of the FMOD event

    [FMODUnity.EventRef]
    public string ScribbleString = "event:/Scribble"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance ScribbleEv; //creating an instance of the FMOD event

    public List<string> traits;
    public List<GameObject> inventory;

    public bool dispensor;
    public bool tampered;
    public bool cuttable;

    public bool dirty;
    public bool electronic;
    public bool flammable;

    public bool canInteract = false;

    public string objectName;
    public int objectValue;

    public Engagement engagement;


    Renderer rend;
    public Material CanInteractGlow;
    public Material defaultMat;

    public Sprite WrittenSprite;
    public Sprite BurnedSprite;
    public Sprite CutSprite;
    public Sprite WetSprite;

    

    void Start()
    {
        PourEv = FMODUnity.RuntimeManager.CreateInstance(pourString); //tell the instance to use the hidingSpotString (event)
        FireIgniteEv = FMODUnity.RuntimeManager.CreateInstance(FireIgniteString); //tell the instance to use the hidingSpotString (event)
        FireSustainEv = FMODUnity.RuntimeManager.CreateInstance(FireSustainString); //tell the instance to use the hidingSpotString (event)
        CarveBoardEv = FMODUnity.RuntimeManager.CreateInstance(CarveBoardString); //tell the instance to use the hidingSpotString (event)
        ScribbleEv = FMODUnity.RuntimeManager.CreateInstance(ScribbleString); //tell the instance to use the hidingSpotString (event)

        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = defaultMat;

    }


    void Update()
    {



    }

    public void Interact()
    {
        //is this where the player would press E to interact w/the stationary object & I would put the SFX here? -Drew
        //currently I've put it within the boolean function below so everytime you step into the area & the material turns
        //green, the sound triggers
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            
            foreach (GameObject item in InventoryManager.instance.items)
            {
                Debug.Log("Checking");
                if (CheckIfCompatible(item.GetComponent<Item>()))
                {
                    Debug.Log("Success");
                    rend.sharedMaterial = CanInteractGlow;
                    canInteract = true;
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            rend.sharedMaterial = defaultMat;
        }

    }
    public bool CheckIfCompatible(Item pickup)
    {


        if (pickup.writing && dirty == true)
        {
            ScribbleEv.start();
            return true;
        }
        if (pickup.sharp && (cuttable == true || electronic == true))
        {
            CarveBoardEv.start();
            return true;
            
        }
        if (pickup.liquid && (dirty || electronic == true || flammable == true))
        {
            //FMOD Event here slow pour
            PourEv.start();
            return true;
        }
        if (pickup.ignitor && flammable == true)
        {
            FireIgniteEv.start();
            FireSustainEv.start();
            return true;
        }
        else
        {
            return false;
        }
    }
}
