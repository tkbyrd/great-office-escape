﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class hiding : MonoBehaviour {

    private bool hidden = false;
    PlayerMovement player;
    private bool inside = false;
    private Transform bp;
    private bool discovered = false;

    public string hidingSpotType; //variable that you assign w/in Inspector
    //the lines below need to be included w/in every script that requires sound to be played (parameter instance is optional/varies upon need)
    [FMODUnity.EventRef]
    public string hidingSpotString = "event:/HidingSpotSFX"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance hidingSpotEv; //creating an instance of the FMOD event
    FMOD.Studio.ParameterInstance hidingSpotLocation;

    // Use this for initialization
    void Start () {
        hidingSpotEv = FMODUnity.RuntimeManager.CreateInstance(hidingSpotString); //tell the instance to use the hidingSpotString (event)
        hidingSpotEv.getParameter("HidingSpot", out hidingSpotLocation);//when you have a parameter for different scenarios e.g. different hiding places

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        bp = transform.Find("bp");
    }
	
	// Update is called once per frame
	void Update () {
        if (hidden)
        {
            transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "'Space' to get out";
        }
        else {
            transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "'Space' to hide";
        }

        //i'll consolidate this later; just checking if this works - Drew
        if (Input.GetKeyDown("space") && inside && hidingSpotType == "Plant") //something w/tags got messed up
        {
            //FindObjectOfType<AudioManager>().Play("PlantEnter");
            hidingSpotLocation.setValue(0); //hiding spot value within FMOD (parameter value)
            hidingSpotEv.start(); //play the sound
            player.ToggleHidden();
            hidden = !hidden;   
        }
        else if (Input.GetKeyDown("space") && inside && hidingSpotType == "Window")
        {
            if (hidden == false)
            {
                //FindObjectOfType<AudioManager>().Play("WindowEnter");
                hidingSpotLocation.setValue(4); //hiding spot value within FMOD (parameter value)
                hidingSpotEv.start(); //play the sound
                player.ToggleHidden();
                hidden = !hidden;
            }

            else if (hidden == true)
            {
                //FindObjectOfType<AudioManager>().Play("WindowExit");
                hidingSpotLocation.setValue(3); //hiding spot value within FMOD (parameter value)
                hidingSpotEv.start(); //play the sound
                player.ToggleHidden();
                hidden = !hidden;
            }
            
        }
        else if (Input.GetKeyDown("space") && inside && hidingSpotType == "Locker")
        {
            if (hidden == false)
            {
                //FindObjectOfType<AudioManager>().Play("LockerEnter");
                hidingSpotLocation.setValue(1); //hiding spot value within FMOD (parameter value)
                hidingSpotEv.start(); //play the sound
                player.ToggleHidden();
                hidden = !hidden;
            }

            else if (hidden == true)
            {
                //FindObjectOfType<AudioManager>().Play("LockerExit");
                hidingSpotLocation.setValue(2); //hiding spot value within FMOD (parameter value)
                hidingSpotEv.start(); //play the sound
                player.ToggleHidden();
                hidden = !hidden;
            }
            
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!discovered && col.gameObject.tag == "Player")
        {
            bp.GetComponent<SpriteRenderer>().enabled = true;
            discovered = true;
        }
        if (col.gameObject.tag == "Player")
        {
            transform.GetChild(0).gameObject.SetActive(true);
            inside = true;
            
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            transform.GetChild(0).gameObject.SetActive(false);
            inside = false;
        }
    }
}
