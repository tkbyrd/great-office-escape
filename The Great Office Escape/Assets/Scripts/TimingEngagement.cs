﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimingEngagement : Engagement {

    public float health = 0f;
    public float origHealthRate = 55f;
    private float healthRate;
    public float rateRange = 5f;

    private bool direction = true;
    private bool active = false;

    Slider slider;
    Gradient gradient;
    GradientColorKey[] colorKey;
    GradientAlphaKey[] alphaKey;

    //SFX
    [FMODUnity.EventRef]
    public string toiletSFXstring = "event:/ToiletSFX"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance toiletEv; //creating an instance of the FMOD event
    //FMOD.Studio.ParameterInstance toiletSFXtype;

    // Use this for initialization
    void Start ()
    {
        toiletEv = FMODUnity.RuntimeManager.CreateInstance(toiletSFXstring); //tell the instance to use the hidingSpotString (event)
        //toiletEv.getParameter("HidingSpot", out toiletSFXtype);//when you have a parameter for different scenarios e.g. different hiding places

        base.Start();

        healthRate = origHealthRate;

        gradient = new Gradient();

        colorKey = new GradientColorKey[4];
        colorKey[0].color = Color.red;
        colorKey[0].time = 0.0f;
        colorKey[1].color = new Color(1.0F, 0.65F, 0F);
        colorKey[1].time = 0.45f;
        colorKey[2].color = Color.yellow;
        colorKey[2].time = 0.92f;
        colorKey[3].color = Color.green;
        colorKey[3].time = .93f;

        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKeyDown("e") && InRange == true && stationScript.canInteract == true)
        {
            print("Player is trying to clog toilet");
            
            if (health > 92f)
            {
                //play environmental sound: WaterOverflow
                toiletEv.start();
                transform.GetChild(1).gameObject.SetActive(true);
                transform.GetChild(0).gameObject.SetActive(false);
            }

        }


        slider = transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Slider>();
        slider.value = health;
        transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().color = gradient.Evaluate(slider.value / 100f);

        if (!base.GetDestroyedStatus() && active)
        {
            health += Time.deltaTime * healthRate;
            if (health > 100f || health < 0f)
            {
                if (health > 100f)
                {
                    direction = false;
                }
                else if (health < 0)
                {
                    direction = true;
                }

                healthRate = Random.Range(origHealthRate - rateRange, origHealthRate + rateRange);
                if (!direction)
                {
                    healthRate = -healthRate;
                }

            }

        }
    }

    public override void Interact()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.gameObject.tag == "Player" && !base.GetDestroyedStatus())
        {
            transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text = "Press E when green!";
            transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
            health = 0;
            active = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        if (col.gameObject.tag == "Player")
        {
            transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
            active = false;
        }
    }
}
