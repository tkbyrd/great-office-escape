﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestrictedArea : MonoBehaviour {

    public AudioManager audioManager;
    public GameManager gameManager;
    private PlayerSuspicion player = null;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Triggers when any object enters this object's range of detection.
    public void OnTriggerEnter2D(Collider2D collision)
    {
        //If the colliding object detected is tagged as "player"...
        if (collision.gameObject.tag == "Player")
        {
            player = collision.GetComponent<PlayerSuspicion>(); //Set the colliding object to a PlayerMovement variable.
            player.playerIsSuspicious = true;
            print("Player is now in a restricted area.");
        }
    }

    //Triggers when any object exits this object's range of detection.
    public void OnTriggerExit2D(Collider2D collision)
    {
        //If the colliding object detected is tagged as "player"...
        if (collision.gameObject.tag == "Player")
        {
            player = collision.GetComponent<PlayerSuspicion>(); //Set the colliding object to a PlayerMovement variable.
            player.playerIsSuspicious = false;
        }
    }

}
