﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tp : MonoBehaviour {

    PlayerMovement playerMovement;
    private int localTp;
    private CircleCollider2D playerCollider;
    BoxCollider2D tpCollider;

    // Use this for initialization
    void Start () {
        playerMovement = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
        playerCollider = GameObject.FindWithTag("Player").GetComponent<CircleCollider2D>();
        tpCollider = transform.GetComponent<BoxCollider2D>();

    }
	
	// Update is called once per frame
	void Update () {
        localTp = playerMovement.tp;
        if (localTp < 2)
        {
            if (playerCollider.bounds.Intersects(tpCollider.bounds) && Input.GetKeyDown("r"))
            {
                playerMovement.tp += 1;
            }
        }
        else {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" && localTp < 2)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            
        }
        
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
