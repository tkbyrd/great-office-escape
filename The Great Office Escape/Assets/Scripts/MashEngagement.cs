﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MashEngagement : Engagement {

    public float health = 100f;
    public float healthRate = 15f;



    public Slider slider;
    Gradient gradient;
    GradientColorKey[] colorKey;
    GradientAlphaKey[] alphaKey;


    [FMODUnity.EventRef]
    public string printerSFXstring = "event:/PrinterSFX"; //what you use in Unity to reference the event w/in FMOD
    FMOD.Studio.EventInstance printerEv; //creating an instance of the FMOD event
    FMOD.Studio.ParameterInstance printerSFXtype;



    // Use this for initialization
    void Start ()
    {
        printerEv = FMODUnity.RuntimeManager.CreateInstance(printerSFXstring); //tell the instance to use the hidingSpotString (event)
        printerEv.getParameter("HidingSpot", out printerSFXtype);//when you have a parameter for different scenarios e.g. different hiding places

        base.Start();

        gradient = new Gradient();

        colorKey = new GradientColorKey[4];
        colorKey[0].color = Color.red;
        colorKey[0].time = 0.05f;
        colorKey[1].color = new Color(1.0F, 0.65F, 0F);
        colorKey[1].time = 0.35f;
        colorKey[2].color = Color.yellow;
        colorKey[2].time = 0.65f;
        colorKey[3].color = Color.green;
        colorKey[3].time = 0.95f;

        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);
    }
	
	// Update is called once per frame
	void Update ()
    {
        /*if (GetComponent<InventoryManager>().HasItem("stapler"))
        {
            print(GetComponent<InventoryManager>().HasItem("stapler"));
            canBeDestroyed = true;

        }
        else
        {
            canBeDestroyed = false;
        }*/



        slider = transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Slider>();
        slider.value = health;
        transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().color = gradient.Evaluate(slider.value / 100f);
        if (health < 100f && !base.GetDestroyedStatus() && health > 0)
        {
            health += Time.deltaTime * healthRate;
        }
        else if (health <= 0 && !base.GetDestroyedStatus())
        {
            //FMOD event trigger here
            printerSFXtype.setValue(0); //hiding spot value within FMOD (parameter value)
            printerEv.start(); //play the sound

            base.SetDestroyedStatus(true);
            base.mapAnim.enabled = false;
            base.map.transform.localScale = new Vector3(.8f, .8f, 0);

            transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
            transform.GetChild(0).GetChild(0).gameObject.SetActive(false);

            gameManager.score += GetComponent<StationaryObject>().objectValue;
            

        }
        else
        {

            health = 100f;
        }

        if(InRange && health < 100f)
        {
            base.playerCollider.GetComponent<PlayerSuspicion>().playerIsSuspicious = true;
        }

        


        if (Input.GetKeyDown(KeyCode.E) && base.InRange == true && stationScript.canInteract == true)
        {
            
            health -= 5f;
            Debug.Log(health);
        }
    }

    /*public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            InRange = false;
            canvas.SetActive(false);
        }
    }*/


    override public void Interact()
    {

    }
}
