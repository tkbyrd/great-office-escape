﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour {

    public AudioManager audioManager;
    public GameManager gameManager;
    public string objectName;
    public int objectValue;
    
    private bool playerNearby = false;
    private PlayerMovement player = null;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        CheckInput();
    }

    //Performs various functions specific to each object - needs overridden when new objects are made.
    public void Interact(PlayerMovement player)
    {
        //Call this object's interacion script - should be an attached class, make an abstract so then individual scripts can be used for different "minigames"
        //Make sure to increase the score when applicable
    }


    //Is called to check if this object should be interacted with.
    public void CheckInput()
    {
        //If the player is nearby and they are hitting x...
        if (playerNearby && Input.GetKey("x"))
        {
            Interact(player); //Call this object's interact function to perform the designated task.
        }
    }

    //Triggers when any object enters this object's range of detection.
    public void OnTriggerEnter2D(Collider2D collision)
    {
        //If the colliding object detected is tagged as "player"...
        if (collision.gameObject.tag == "Player")
        {
            player = collision.GetComponent<PlayerMovement>(); //Set the colliding object to a PlayerMovement variable.
            playerNearby = true; //Indicate that this object is within the player's reach by setting playerNearby to true.
        }
    }

    //Triggers when any object exits this object's range of detection.
    public void OnTriggerExit2D(Collider2D other)
    {
        //If the colliding object detected is tagged as "player"...
        if (other.gameObject.tag == "Player")
        {
            player = null; //Set the colliding object to null - the player is no longer nearby.
            playerNearby = false; //Indicate that this object is no longer within the player's reach by setting playerNearby to true.
        }
    }
}
